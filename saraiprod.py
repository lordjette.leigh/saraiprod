from app import create_app, db, cli
from app.models import User, Post, Message, Notification, Task, FilesFiles, \
    PostLike, CommunitiesCommunity, ContentRecommendation, PopularityRecommendation, \
    RecordStat, SmtpLogs, PersonalizedNotificationInfo, RecordsRecordsMaterialOrPublicationType

app = create_app()
cli.register(app)


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post, 'Message': Message,
            'Notification': Notification, 'Task': Task, 'FilesFiles':FilesFiles,
            'PostLike': PostLike, 'CommunitiesCommunity': CommunitiesCommunity,
            'ContentRecommendation': ContentRecommendation, 'PopularityRecommendation': PopularityRecommendation,
            'RecordStat': RecordStat, 'SmtpLogs': SmtpLogs, 'PersonalizedNotificationInfo': PersonalizedNotificationInfo,
            'RecordsRecordsMaterialOrPublicationType': RecordsRecordsMaterialOrPublicationType }


