FROM python:3.8-alpine

RUN adduser -D saraiprod

WORKDIR /home/saraiprod


# Install dependencies
RUN apt-get update \
    && apt-get install python-setuptools \
    && apt-get -qy upgrade --fix-missing --no-install-recommends \
    && apt-get -qy install --fix-missing --no-install-recommends
#    && easy_install supervisor


COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn pymysql

COPY app app
COPY migrations migrations
COPY saraiprod.py config.py boot.sh ./
RUN chmod a+x boot.sh

ENV FLASK_APP saraiprod.py

RUN chown -R saraiprod:saraiprod ./
USER saraiprod

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
