import base64
import pytz, datetime as dtz
from datetime import datetime, timedelta
from hashlib import md5
import json
import os
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
import redis
import rq
from app import db, login
from app.search import add_to_index, remove_from_index, query_index
from sqlalchemy.dialects.postgresql import ARRAY, DATERANGE, BIGINT, UUID, SMALLINT, NUMERIC, JSONB
import uuid as uuid_r




class SearchableMixin(object):

    @classmethod
    def search(cls, expression, page, per_page):
        ids, total = query_index(cls.__tablename__, expression, page, per_page)
        if total == 0:
            return cls.query.filter_by(id=0), 0
        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        return cls.query.filter(cls.id.in_(ids)).order_by(
            db.case(when, value=cls.id)), total


    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add': list(session.new),
            'update': list(session.dirty),
            'delete': list(session.deleted)
        }

    @classmethod
    def after_commit(cls, session):
        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)


db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)


class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page,
                                **kwargs),
                'next': url_for(endpoint, page=page + 1, per_page=per_page,
                                **kwargs) if resources.has_next else None,
                'prev': url_for(endpoint, page=page - 1, per_page=per_page,
                                **kwargs) if resources.has_prev else None
            }
        }
        return data



def LocalTimezoneManilaModelFunc():
    return dtz.datetime.strftime(pytz.utc.localize(dtz.datetime.utcnow())\
        .astimezone(pytz.timezone("Asia/Manila")), '%Y-%m-%d %H:%M:%S.%f')


followers = db.Table(
    'followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)


class User(UserMixin, PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), lazy='dynamic')
    messages_sent = db.relationship('Message',
                                    foreign_keys='Message.sender_id',
                                    backref='author', lazy='dynamic')
    messages_received = db.relationship('Message',
                                        foreign_keys='Message.recipient_id',
                                        backref='recipient', lazy='dynamic')
    last_message_read_time = db.Column(db.DateTime)
    notifications = db.relationship('Notification', backref='user',
                                    lazy='dynamic')
    tasks = db.relationship('Task', backref='user', lazy='dynamic')
    communities_community_au_attr = db.relationship("CommunitiesCommunity", back_populates="accounts_user_cc_attr", lazy='selectin')
    user_personalized_notif_attr = db.relationship("PersonalizedNotificationInfo", back_populates="notif_personalized_attr", lazy='selectin')

    liked = db.relationship('PostLike', foreign_keys='PostLike.user_id', backref='user', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        followed = Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
                followers.c.follower_id == self.id)
        own = Post.query.filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def new_messages(self):
        last_read_time = self.last_message_read_time or datetime(1900, 1, 1)
        return Message.query.filter_by(recipient=self).filter(
            Message.timestamp > last_read_time).count()

    def add_notification(self, name, data):
        self.notifications.filter_by(name=name).delete()
        n = Notification(name=name, payload_json=json.dumps(data), user=self)
        db.session.add(n)
        return n

    def launch_task(self, name, description, *args, **kwargs):
        rq_job = current_app.task_queue.enqueue('app.tasks.' + name, self.id,
                                                *args, **kwargs)
        task = Task(id=rq_job.get_id(), name=name, description=description,
                    user=self)
        db.session.add(task)
        return task

    def get_tasks_in_progress(self):
        return Task.query.filter_by(user=self, complete=False).all()

    def get_task_in_progress(self, name):
        return Task.query.filter_by(name=name, user=self,
                                    complete=False).first()

    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'last_seen': self.last_seen.isoformat() + 'Z',
            'about_me': self.about_me,
            'post_count': self.posts.count(),
            'follower_count': self.followers.count(),
            'followed_count': self.followed.count(),
            '_links': {
                'self': url_for('api.get_user', id=self.id),
                'followers': url_for('api.get_followers', id=self.id),
                'followed': url_for('api.get_followed', id=self.id),
                'avatar': self.avatar(128)
            }
        }
        if include_email:
            data['email'] = self.email
        return data

    def from_dict(self, data, new_user=False):
        for field in ['username', 'email', 'about_me']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user



    def like_post(self, post):
        if not self.has_liked_post(post):
            like = PostLike(user_id=self.id, post_id=post.id)
            db.session.add(like)

    def unlike_post(self, post):
        if self.has_liked_post(post):
            PostLike.query.filter_by(
                user_id=self.id,
                post_id=post.id).delete()

    def has_liked_post(self, post):
        return PostLike.query.filter(
            PostLike.user_id == self.id,
            PostLike.post_id == post.id).count() > 0




@login.user_loader
def load_user(id):
    return User.query.get(int(id))



class Post(SearchableMixin, db.Model):
    __searchable__ = ['community_type','material_publication_type','title','author_name','body','publication_date',\
            'call_number','physical_desc','content_book_serial','physical_ext_page','biography_note','catalog_source',\
            'circulation_no','accession_no','bersyon','location','status','additional_notes','keywords','access_right',\
            'timestamp','user_id', 'language']
    id = db.Column(db.Integer, primary_key=True)
    community_type = db.Column(ARRAY(db.Text), default='Others', nullable=False)
    material_publication_type = db.Column(db.String(100))
    title = db.Column(db.String(255), nullable=False)
    author_name = db.Column(db.String(255), default='', nullable=False)
    body = db.Column(db.TEXT, default='', nullable=False)
    publication_date = db.Column(db.DateTime,  default=datetime.utcnow)
    call_number = db.Column(db.String(100), default='', nullable=False)
    physical_desc = db.Column(db.TEXT, default='', nullable=False)
    content_book_serial = db.Column(db.TEXT, default='', nullable=False)
    physical_ext_page = db.Column(db.TEXT, default='', nullable=False)
    biography_note = db.Column(db.TEXT, default='', nullable=False)
    catalog_source = db.Column(db.String(100), default='', nullable=False)
    circulation_no = db.Column(db.String(100), default='', nullable=False)
    accession_no = db.Column(db.String(100), default='', nullable=False)
    bersyon = db.Column(db.String(100), default='1', nullable=False)
    location = db.Column(db.String(255), default='', nullable=False)
    status = db.Column(db.String(20), default='', nullable=False)
    additional_notes = db.Column(db.TEXT, default='', nullable=False)
    keywords = db.Column(db.TEXT, default='', nullable=False)
    access_right = db.Column(db.String(50), nullable=False)
    timestamp = db.Column(db.DateTime, index=True, default=LocalTimezoneManilaModelFunc())
    modified_date = db.Column(db.DateTime, nullable=False, default=LocalTimezoneManilaModelFunc())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    language = db.Column(db.String(5))

    likes = db.relationship('PostLike', backref='post', lazy='dynamic')

    def __repr__(self):
        return '<Post {}>'.format(self.body)


class PostLike(db.Model):
    __tablename__ = 'post_like'
    id = db.Column(BIGINT, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))

    def __repr__(self):
        return f"PostLike('{self.id}','{self.user_id}','{self.post_id}')"


class FilesFiles(db.Model):
    created = db.Column(db.DateTime, nullable=False, default=LocalTimezoneManilaModelFunc())
    updated = db.Column(db.DateTime, nullable=False, default=LocalTimezoneManilaModelFunc())
    id = db.Column(UUID(as_uuid=True), primary_key=True, nullable=False, default=uuid_r.uuid4())
    uri = db.Column(db.TEXT,  nullable=False)
    storage_class = db.Column(db.String(1), default='')
    key= db.Column(db.Text, default='', nullable=False)
    version_id = db.Column(UUID(as_uuid=True), primary_key=True, nullable=False)
    _mimetype = db.Column(db.String(255), default='', index=True)
    is_head= db.Column(db.Boolean, nullable=False, default=True)
    size = db.Column(BIGINT)
    checksum = db.Column(db.String(255), default='')
    readable = db.Column(db.Boolean, nullable=False, default=True)
    writable = db.Column(db.Boolean, nullable=False, default=False)
    last_check_at = db.Column(db.DateTime, nullable=False, default='')
    last_check = db.Column(db.Boolean, default=True)
    record_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    def __repr__(self):
        return f"FilesFiles('{self.id}')"


class CommunitiesCommunity(db.Model):
    created = db.Column(db.DateTime, nullable=False, default=LocalTimezoneManilaModelFunc())
    updated = db.Column(db.DateTime, nullable=False, default=LocalTimezoneManilaModelFunc())
    id = db.Column(db.String(100), primary_key=True, default='', nullable=False)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    title = db.Column(db.String(255), default='', nullable=False)
    description = db.Column(db.TEXT, default='', nullable=False)
    page = db.Column(db.TEXT, default='', nullable=False)
    curation_policy = db.Column(db.TEXT, default='', nullable=False)
    last_record_accepted = db.Column(db.DateTime,default=LocalTimezoneManilaModelFunc(),nullable=False)
    logo_ext = db.Column(db.String(4), default='')
    ranking = db.Column(db.Integer, nullable=False, default=0 )
    fixed_points = db.Column(db.Integer, nullable=False, default=0 )
    deleted_at = db.Column(db.DateTime, default=LocalTimezoneManilaModelFunc())
    accounts_user_cc_attr = db.relationship("User", back_populates="communities_community_au_attr", lazy='selectin')
    def __repr__(self):
        return f"CommunitiesCommunity('{self.id}','{self.title}')"


class ContentRecommendation(db.Model):
    uuid = db.Column(db.String(50), primary_key=True, default=uuid_r.uuid4(), nullable=False)
    record_id = db.Column(BIGINT)
    recommended_content_id = db.Column(db.TEXT)
    def __repr__(self):
        return f"ContentRecommendation('{self.recommended_content_id}')"

class PopularityRecommendation(db.Model):
    uuid = db.Column(db.String(50), primary_key=True, default=uuid_r.uuid4(), nullable=False)
    click_article_id = db.Column(BIGINT)
    total_click_environment = db.Column(BIGINT)
    date = db.Column(db.Integer)
    def __repr__(self):
        return f"PopularityRecommendation('{self.total_click_environment}')"

class RecordStat(db.Model):
    record_id = db.Column(db.Integer, db.ForeignKey('post.id'), primary_key=True, nullable=False)
    total_views = db.Column(BIGINT)
    total_likes = db.Column(BIGINT)
    def __repr__(self):
        return f"RecordStat('{self.total_views}', '{self.total_likes}')"


class SmtpLogs(db.Model):
    uuid = db.Column(db.String(50), primary_key=True, default=uuid_r.uuid4(), nullable=False)
    user_id = db.Column(BIGINT)
    email = db.Column(db.String(200), default='no email', nullable=False)
    rcode = db.Column(BIGINT)
    timestamp = db.Column(db.DateTime, default=LocalTimezoneManilaModelFunc(), nullable=False)
    status = db.Column(db.String(100), default='sent', nullable=False)
    execution_date = db.Column(db.DateTime)
    def __repr__(self):
        return f"SmtpLogs('{self.user_id}','{self.rcode}')"


class PersonalizedNotificationInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    community_type = db.Column(ARRAY(db.Text), default=['all'], nullable=False)
    material_publication_type = db.Column(db.String(100), default='all', nullable=False)
    activate_pn = db.Column(db.String(50), default='y', nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    notif_personalized_attr = db.relationship("User", back_populates="user_personalized_notif_attr", lazy='selectin')
    def __repr__(self):
        return f"PersonalizedNotificationInfo('{self.community_type}','{self.material_publication_type}')"


class RecordsRecordsMaterialOrPublicationType(db.Model):
    __tablename__ = "records_records_material_or_publication_type"
    id = db.Column(db.Integer, primary_key=True)
    material_publication_type = db.Column(db.String(100))
    definition = db.Column(db.TEXT)
    created = db.Column(db.DateTime, default=LocalTimezoneManilaModelFunc())
    updated = db.Column(db.DateTime, default=LocalTimezoneManilaModelFunc())
    deleted_at = db.Column(db.DateTime)
    deleted = db.Column(db.Boolean, default=False)
    def __repr__(self):
        return f"RecordsRecordsMaterialOrPublicationType('{self.material_publication_type}','{self.definition}')"

class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    recipient_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Message {}>'.format(self.body)


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    timestamp = db.Column(db.Float, index=True, default=time)
    payload_json = db.Column(db.Text)

    def get_data(self):
        return json.loads(str(self.payload_json))


class Task(db.Model):
    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(128), index=True)
    description = db.Column(db.String(128))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    complete = db.Column(db.Boolean, default=False)

    def get_rq_job(self):
        try:
            rq_job = rq.job.Job.fetch(self.id, connection=current_app.redis)
        except (redis.exceptions.RedisError, rq.exceptions.NoSuchJobError):
            return None
        return rq_job

    def get_progress(self):
        job = self.get_rq_job()
        return job.meta.get('progress', 0) if job is not None else 100
