from datetime import date
from flask import request
from flask_wtf import FlaskForm
from wtforms import  StringField, PasswordField, IntegerField, FormField, BooleanField, SubmitField, TextAreaField, DateTimeField, RadioField, SelectField
from flask_wtf.file import FileField, FileRequired
from wtforms.fields.html5 import URLField, TelField, SearchField, DateTimeLocalField
from wtforms.validators import ValidationError, DataRequired, Length, Email, EqualTo, Regexp, InputRequired
from flask_babel import _, lazy_gettext as _l
from app.models import User



class EditProfileForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    about_me = TextAreaField(_l('About me'),
                             validators=[Length(min=0, max=140)])
    submit = SubmitField(_l('Submit'))

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError(_('Please use a different username.'))


class EmptyForm(FlaskForm):
    submit = SubmitField('Submit')


class DepositFileForm(FlaskForm):
    file = FileField('Choose file', validators=[FileRequired()], render_kw={"required":"", "title": "Select file to upload"})
    upload_btn = SubmitField('Save')

class PostForm(FlaskForm):
    community = SelectField("Community", coerce=str, validators=[InputRequired()], render_kw={"class":"form-control selectpicker", "data-live-search":"true", "title": "Select Communities", "multiple":""})
    material_pub_rdo_btn = RadioField("Material Type", coerce=str, validators=[InputRequired()], render_kw={"title": "Select Material Type"}) 
    sarai_title = StringField('Title', validators=[InputRequired()], render_kw={"placeholder": "", "title": "Please type title"})
    creators = StringField('Author', validators=[DataRequired()], render_kw={"placeholder": "Full name of organization author", "title": "Write the author(s) name"})
    post = TextAreaField(_l('Summary or Description'), validators=[DataRequired()])
    publication_date = DateTimeField('Publication Date', format="%Y-%m-%d", default=date.today(), \
            validators=[DataRequired(message="Invalid type, expected string")])
    call_number = StringField("Call Number", render_kw={"placeholder": ""})
    physical_description = TextAreaField("Physical Description", render_kw={"placeholder":"" })
    content_book_serial = TextAreaField("Content of Book/Serial", render_kw={"placeholder":"" })
    physical_extension_page = TextAreaField("Physical Extension Page", render_kw={"placeholder":"" })
    bibliographic_note = TextAreaField("Bibliographic Note", render_kw={"placeholder":"" })
    catalog_source = StringField("Catalog Source", render_kw={"placeholder": ""})
    circulation_number = StringField("Circulation Number", render_kw={"placeholder": ""})
    accession_number = StringField("Accession Number", render_kw={"placeholder": ""})
    bersyon = StringField("Version", render_kw={"placeholder": ""})
    location = StringField("Location", render_kw={"placeholder": ""})
    status = StringField("Status", render_kw={"placeholder": "e.g. IN"})
    additional_notes = TextAreaField("Additional Notes", render_kw={"placeholder":"" })
    keywords = StringField("Keywords", render_kw={"placeholder": ""})
    access_right = RadioField('Access Right', coerce=str, choices=[('open','Open Access'),('embargoed','Embargoed Access'),('restricted','Restricted Access'),('closed','Closed Access')], validators=[InputRequired()])
    # validate_choice=False,
    #reserve_doi_btn = SubmitField('Reserved PID', render_kw={'disabled':''})
    submit = SubmitField(_l('Submit'))


class SearchForm(FlaskForm):
    q = StringField(_l('Search'), validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)


class MessageForm(FlaskForm):
    message = TextAreaField(_l('Message'), validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField(_l('Submit'))


class PersonalizedNotificationForm(FlaskForm):
    community_pn = SelectField("Community", coerce=str, validators=[DataRequired()], \
        render_kw={"class":"form-control selectpicker", "data-live-search":"true", "title": "Select Communities", "multiple":""})
    material_pub_rdo_btn_pn = RadioField("Material Type", coerce=str, validators=[InputRequired()], render_kw={"title": "Select Material Type"})
    activate_pn = RadioField('Activate Notification?', validators=[InputRequired()], choices=[('y','Yes'),('n','No')])
    submit = SubmitField(_l('Save'), render_kw={"class": "btn btn-info btn-sm mt-1 mb-1"})
