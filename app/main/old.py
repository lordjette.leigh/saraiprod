            # try:

            #     u_record_bucket_exist = (
            #         update(RecordsBuckets)
            #         .where(RecordsBuckets.c.record_id == cast(literal(post_id),BIGINT))\
            #         .values(record_id=RecordsBuckets.c.record_id, bucket_id=RecordsBuckets.c.bucket_id )
            #         .returning(literal(1))
            #         .cte('u_record_bucket_exist')
            #     )


            #     i_records_bucket = (
            #         insert(RecordsBuckets).from_select([RecordsBuckets.c.record_id,RecordsBuckets.c.bucket_id]\
            #         ,select([cast(literal(post_id),BIGINT).label("record_id"),cast(literal(b_uuid),UUID).label("bucket_id")]) \
            #         .where(exists(u_record_bucket_exist.select()))
            #         )
            #         .returning(RecordsBuckets.c.record_id,RecordsBuckets.c.bucket_id)
            #         .cte("i_records_bucket")
                    
            #     )


            #     static_id = (
            #         select([
            #                 literal(LocalTimezoneManilaModelFunc()).label("created"), \
            #                 literal(LocalTimezoneManilaModelFunc()).label("updated"), \
            #                 i_records_bucket.c.record_id, \
            #                 i_records_bucket.c.bucket_id, \
            #                 cast(literal(f_uuid),UUID).label("file_id"), \
            #                 literal(v_uuid).label("version_id"), \
            #                 func.concat('', func.replace(literal(FileDirectory),'.', '') ).label("uri"), \
            #                 literal(storage_cl).label('storage_class'), \
            #                 literal(size).label('size'), \
            #                 literal(checksum_uuid).label('checksum'), \
            #                 literal(ff_readable).label('readable'), \
            #                 literal(ff_writable).label('writable'), \
            #                 literal(LocalTimezoneManilaModelFunc()).label('last_check_at'), \
            #                 literal(ff_last_check).label('last_check'), \
            #                 literal(QUOTA_AND_MAX_SIZE).label('quota_size'), \
            #                 literal(QUOTA_AND_MAX_SIZE).label('max_file_size'), \
            #                 literal(1).label('default_location'), \
            #                 literal(filename).label('key'), \
            #                 literal(mimetype).label('_mimetype'),\
            #                 literal(fb_locked).label('locked'), \
            #                 literal(fb_deleted).label('deleted')             
            #         ])
            #     ).cte("static_id" )


            #     i_to_files_files = (
            #         insert(FilesFiles).from_select( [FilesFiles.created,FilesFiles.updated,FilesFiles.id,FilesFiles.uri,FilesFiles.storage_class,\
            #             FilesFiles.size,FilesFiles.checksum,FilesFiles.readable,FilesFiles.writable,FilesFiles.last_check_at,FilesFiles.last_check]\
            #         ,select([literal(LocalTimezoneManilaModelFunc()),literal(LocalTimezoneManilaModelFunc()),static_id.c.file_id,static_id.c.uri,static_id.c.storage_class,static_id.c.size,\
            #             static_id.c.checksum,static_id.c.readable,static_id.c.writable,literal(LocalTimezoneManilaModelFunc()),static_id.c.last_check]) \
            #             .where(static_id.c.size != 0)
            #         )
            #         .returning(FilesFiles.created,FilesFiles.updated,FilesFiles.id,FilesFiles.uri,FilesFiles.storage_class,\
            #             FilesFiles.size,FilesFiles.checksum,FilesFiles.readable,FilesFiles.writable,FilesFiles.last_check_at,FilesFiles.last_check)
            #         .cte("i_to_files_files")
            #     )

            #     # get_bucket_e = (
            #     #     select([RecordsBucket.bucket_id]).where(RecordsBucket.record_id == cast(literal(post_id),BIGINT))
            #     # ).cte("get_bucket_e" )



            #     u_files_bucket_cte = (
            #         update(FilesBucket)
            #         .where(and_(FilesBucket.id == i_records_bucket.c.bucket_id,
            #                     FilesBucket.default_location == 1)
            #         )
            #         .values(size=FilesBucket.size + size)
            #         .returning(literal(1))\
            #         .cte('u_files_bucket_cte')
            #     )

                
            #     i_files_bucket = (
            #         insert(FilesBucket).from_select([
            #             FilesBucket.created,FilesBucket.updated,FilesBucket.id,FilesBucket.default_location,FilesBucket.default_storage_class,\
            #             FilesBucket.size,FilesBucket.quota_size,FilesBucket.max_file_size,FilesBucket.locked,FilesBucket.deleted ]\
            #         ,select([literal(LocalTimezoneManilaModelFunc()),literal(LocalTimezoneManilaModelFunc()),static_id.c.bucket_id,static_id.c.default_location,static_id.c.storage_class,\
            #             static_id.c.size,static_id.c.quota_size,static_id.c.max_file_size,static_id.c.locked,static_id.c.deleted]) \
            #         .where(~exists(u_files_bucket_cte.select()))
            #         )
            #         .returning(FilesBucket.created,FilesBucket.updated,FilesBucket.id,FilesBucket.default_location,FilesBucket.default_storage_class,\
            #             FilesBucket.size,FilesBucket.quota_size,FilesBucket.max_file_size,FilesBucket.locked,FilesBucket.deleted) 
            #         .cte("i_files_bucket")  
            #     )


            #     i_files_object = (
            #         insert(FilesObject).from_select([
            #             FilesObject.c.created, FilesObject.c.updated,FilesObject.c.bucket_id,FilesObject.c.key,FilesObject.c.version_id,\
            #             FilesObject.c.file_id,FilesObject.c._mimetype,FilesObject.c.is_head] \
            #         ,select([ literal(LocalTimezoneManilaModelFunc()),literal(LocalTimezoneManilaModelFunc()),\
            #             static_id.c.bucket_id, \
            #             static_id.c.key,\
            #             static_id.c.version_id, \
            #             static_id.c.file_id,\
            #             static_id.c._mimetype, \
            #             literal(True)])
            #             .where(
            #                 and_(
            #                 static_id.c.bucket_id == select([i_files_bucket.c.id]), \
            #                 static_id.c.file_id  ==  select([i_to_files_files.c.id])
            #                 )
            #             )
            #         )
            #         .returning(FilesObject.c.created, FilesObject.c.updated,FilesObject.c.bucket_id,FilesObject.c.key,FilesObject.c.version_id,\
            #             FilesObject.c.file_id,FilesObject.c._mimetype,FilesObject.c.is_head)
            #         .cte("i_files_object") 
            #     )

 
            #     returning_files_ = select([i_files_object])
            #     conn.execute(returning_files_)
            #     trans.commit()
            # except:
            #     trans.rollback()
            #     raise