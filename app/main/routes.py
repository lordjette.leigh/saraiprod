import os, json
from datetime import datetime
import pytz, datetime as dtz
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app, abort, flash, send_from_directory, send_file
from flask_login import current_user, login_required
from flask_babel import _, get_locale
from guess_language import guess_language
from app import db
from app.main.forms import EditProfileForm, EmptyForm, PostForm, SearchForm, \
    MessageForm, PersonalizedNotificationForm, DepositFileForm
from app.models import User, Post, Message, Notification, CommunitiesCommunity, RecordStat,\
    RecordsRecordsMaterialOrPublicationType, LocalTimezoneManilaModelFunc, PersonalizedNotificationInfo,\
    ContentRecommendation, FilesFiles, PopularityRecommendation, PostLike
from app.translate import translate
from app.main import bp
import secrets
import random, uuid
import subprocess
from sqlalchemy.sql.expression import func, select, cast
from sqlalchemy import Integer, String, Numeric, Index, select, cast, and_, or_, exists , insert, update, join, cast, Sequence, text

from sqlalchemy.sql.expression import func, literal #and_

from werkzeug.utils import secure_filename
from werkzeug.exceptions import RequestEntityTooLarge

from sqlalchemy.dialects.postgresql import UUID, BIGINT

#CREATE EXTENSION IF NOT EXISTS tablefunc; ##Requirements in postgres

def LocalTimezoneManilaModelFunc():
    return dtz.datetime.strftime(pytz.utc.localize(dtz.datetime.utcnow())\
        .astimezone(pytz.timezone("Asia/Manila")), '%Y-%m-%d %H:%M:%S.%f')


def CreateNewDir():
    UPLOAD_FOLDER_MAIN = current_app.config['UPLOAD_FOLDER']
    global UPLOAD_FOLDER
    UPLOAD_FOLDER = UPLOAD_FOLDER_MAIN + str(random.randint(1,99)) + str('/') + secrets.token_hex(8) 
    cmd="mkdir -p %s && ls -lrt %s"%(UPLOAD_FOLDER,UPLOAD_FOLDER)
    output = subprocess.Popen([cmd], shell=True,  stdout = subprocess.PIPE).communicate()[0]

    if b"total 0" in output:
        print ('Success: Created Directory %s'%(UPLOAD_FOLDER))
    else:
        print ('Failure: Failed to Create a Directory (or) Directory already Exists',UPLOAD_FOLDER)
    
    return UPLOAD_FOLDER

def allowed_file(filename):
    ALLOWED_EXTENSIONS = current_app.config['ALLOWED_EXTENSIONS']
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()
    g.locale = str(get_locale())


@bp.route('/favicon.ico')
def favicon():
    """Return the favicon."""
    return send_from_directory('../static/',
        'favicon.ico', mimetype='image/vnd.microsoft.icon'
    )

@bp.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    #static_logo_folder = url_for('static', filename='img/logo/')
    QUOTA_AND_MAX_SIZE = current_app.config['MAX_CONTENT_LENGTH']
    form = PostForm()
    community_groups = db.session.query(CommunitiesCommunity.id, CommunitiesCommunity.title)\
        .filter(CommunitiesCommunity.deleted_at == None) \
        .order_by(CommunitiesCommunity.title.desc())
    form.community.choices = [(c.id, c.title) for c in community_groups]

    #This is for Dynamic Select Field for Material/Pub Type
    material_groups = db.session.query(RecordsRecordsMaterialOrPublicationType.id, \
        RecordsRecordsMaterialOrPublicationType.material_publication_type)\
        .filter(RecordsRecordsMaterialOrPublicationType.deleted != True) \
        .order_by(RecordsRecordsMaterialOrPublicationType.id.asc())
    form.material_pub_rdo_btn.choices = [(m.material_publication_type, m.material_publication_type) for m in material_groups]

    
    if form.validate_on_submit():
        language = guess_language(form.post.data)
        if language == 'UNKNOWN' or len(language) > 5:
            language = ''

        post = Post(community_type             = form.community.raw_data,\
                    material_publication_type  = form.material_pub_rdo_btn.data,\
                    title                      = form.sarai_title.data,\
                    author_name                = form.creators.data,\
                    body                       = form.post.data,\
                    publication_date           = form.publication_date.data,\
                    call_number                = form.call_number.data,\
                    physical_desc              = form.physical_description.data,\
                    content_book_serial        = form.content_book_serial.data,\
                    physical_ext_page          = form.physical_extension_page.data,\
                    biography_note             = form.bibliographic_note.data,\
                    catalog_source             = form.catalog_source.data,\
                    circulation_no             = form.circulation_number.data,\
                    accession_no               = form.accession_number.data,\
                    bersyon                    = form.bersyon.data,\
                    location                   = form.location.data,\
                    status                     = form.status.data,\
                    additional_notes           = form.additional_notes.data,\
                    keywords                   = form.keywords.data,\
                    access_right               = form.access_right.data,\
                    author                     = current_user,
                    language                   = language)
        db.session.add(post)
        # db.session.flush()

        db.session.commit()
        flash(_('Your post is now published!'))
        return redirect(url_for('main.post', post_id=post.id))
        
    page = request.args.get('page', 1, type=int)
    posts = current_user.followed_posts().paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.upload', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.upload', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template('index.html', title='Post', form=form,
                           posts=posts.items, next_url=next_url,
                           prev_url=prev_url)


@bp.route('/upload_file/<int:post_id>/', methods=['GET', 'POST'])
@login_required
def upload_file(post_id):
    QUOTA_AND_MAX_SIZE = current_app.config['MAX_CONTENT_LENGTH']
    UPLOADED_ASSET_STATIC = current_app.config['UPLOAD_FOLDER']
    post = Post.query.get_or_404(post_id, description=f'No data found with {post_id}. The requested URL was not found \
                        on the server. If you entered the URL manually please check your spelling and try again.')
    uploaded_files = FilesFiles.query.filter_by(record_id=cast(post_id, Integer), storage_class='S').all()

    fileform = DepositFileForm()

    #Configuration
    f_uuid = uuid.UUID(int=random.Random().getrandbits(128))

    #SUBMITTING FILES
    if fileform.validate_on_submit():
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part', 'dark')
            return redirect(request.url)

        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file', 'dark')
            return redirect(request.url)

        if not allowed_file(file.filename):
            flash('Invalid file format', 'warning')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            mimetype = file.content_type
            filename = secure_filename(file.filename)
            


            FileDirectory = CreateNewDir()
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            
            file.seek(0, os.SEEK_END)
            size = file.tell()
            file.close()
            

            _, f_ext = os.path.splitext(file.filename)  # eg. .pdf
            rsplit = filename.rsplit('.', 1)[1].lower() # eg. pdf
         
            print(f'FileDirectory: {FileDirectory}')
            print(f'f_ext: {f_ext}')
            print(f'This is rsplit: {rsplit}')
            print(f'This size: {size}')
            print(f'mimetype: {mimetype}')
            print('This is the filename: ', filename)
            print('ALLOWED', allowed_file(file.filename))

            iff_id = uuid.uuid4()
            iff_vid = uuid.uuid4()
            iff_checksum = uuid.uuid4()
            FILESFILES = FilesFiles(created = LocalTimezoneManilaModelFunc(),\
                                    updated = LocalTimezoneManilaModelFunc(),\
                                    id = iff_id,\
                                    uri = FileDirectory.replace(UPLOADED_ASSET_STATIC, '/'),\
                                    storage_class = 'S',\
                                    key= filename,\
                                    version_id = iff_vid,\
                                    _mimetype = mimetype,\
                                    is_head= True,\
                                    size = size,\
                                    checksum = iff_checksum,\
                                    readable = True,\
                                    writable = False,\
                                    last_check_at = LocalTimezoneManilaModelFunc(),\
                                    last_check =True,\
                                    record_id = post_id,\
                                    user_id = current_user.id )

            db.session.add(FILESFILES)
            db.session.query(Post).filter(Post.id == post_id).update({'modified_date': LocalTimezoneManilaModelFunc()})
            db.session.commit()
            flash('Upload Successful', 'success')

            return redirect(url_for('main.upload_file', post_id=post_id ))

    return render_template('upload_file.html', r_id=post_id, title=post.title, post_author=post.author, fileform=fileform, uploaded_files=uploaded_files, QUOTA_AND_MAX_SIZE=QUOTA_AND_MAX_SIZE)


@bp.route("/upload_file/<int:post_id>/<string:checksum>/delete", methods=['POST', 'GET'])
@login_required
def delete_upload_file(post_id, checksum):
    post = Post.query.get_or_404(post_id)
    uploaded_files = FilesFiles.query.filter_by(record_id=cast(post_id, Integer)).all()
    if post.author != current_user:
        abort(403) 
    if request.method == 'POST':
        db.session.query(FilesFiles).filter(FilesFiles.checksum == checksum).update({'storage_class': 'X'})
        db.session.commit()
        flash('File has been deleted!', 'success')
        return redirect(url_for('main.upload_file', post_id=post_id))



@bp.route("/post/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    form = PostForm()

    #Dynamic Community type selections
    community_groups = db.session.query(CommunitiesCommunity.id, CommunitiesCommunity.title)\
        .filter(CommunitiesCommunity.deleted_at == None) \
        .order_by(CommunitiesCommunity.title.asc())
    form.community.choices = [(c.id, c.title) for c in community_groups]

    #This is for Dynamic Select Field for Material/Pub Type
    material_groups = db.session.query(RecordsRecordsMaterialOrPublicationType.id, \
        RecordsRecordsMaterialOrPublicationType.material_publication_type)\
        .filter(RecordsRecordsMaterialOrPublicationType.deleted != True) \
        .order_by(RecordsRecordsMaterialOrPublicationType.id.asc())
    form.material_pub_rdo_btn.choices = [(m.material_publication_type, m.material_publication_type.title()) for m in material_groups]

    if form.validate_on_submit():
        post.community_type             = request.form.getlist('community')
        post.material_publication_type  = form.material_pub_rdo_btn.data
        post.title                      = form.sarai_title.data
        post.author_name                = form.creators.data
        post.body                       = form.post.data
        post.publication_date           = form.publication_date.data
        post.call_number                = form.call_number.data
        post.physical_desc              = form.physical_description.data
        post.content_book_serial        = form.content_book_serial.data
        post.physical_ext_page          = form.physical_extension_page.data
        post.biography_note             = form.bibliographic_note.data
        post.catalog_source             = form.catalog_source.data
        post.circulation_no             = form.circulation_number.data
        post.accession_no               = form.accession_number.data
        post.bersyon                    = form.bersyon.data
        post.location                   = form.location.data
        post.status                     = form.status.data
        post.additional_notes           = form.additional_notes.data
        post.keywords                   = form.keywords.data
        post.access_right               = form.access_right.data
        post.modified_date              = LocalTimezoneManilaModelFunc()
        db.session.commit()
        flash('Your post has been updated!', 'success')
        return redirect(url_for('main.post', post_id=post.id))
    elif request.method == 'GET':
        form.community.raw_data                 = post.community_type
        form.material_pub_rdo_btn.data          = post.material_publication_type
        form.sarai_title.data                   = post.title
        form.creators.data                      = post.author_name
        form.post.data                          = post.body
        form.publication_date.data              = post.publication_date
        form.call_number.data                   = post.call_number
        form.physical_description.data          = post.physical_desc
        form.content_book_serial.data           = post.content_book_serial
        form.physical_extension_page.data       = post.physical_ext_page
        form.bibliographic_note.data            = post.biography_note
        form.catalog_source.data                = post.catalog_source
        form.circulation_number.data            = post.circulation_no
        form.accession_number.data              = post.accession_no
        form.bersyon.data                       = post.bersyon
        form.location.data                      = post.location
        form.status.data                        = post.status
        form.additional_notes.data              = post.additional_notes
        form.keywords.data                      = post.keywords
        form.access_right.data                  = post.access_right
    return render_template('index.html', title='Update Post', form=form, legend='Update Post')



@bp.route('/like/<int:post_id>/<action>')
@login_required
def like_action(post_id, action):
    post = Post.query.filter_by(id=post_id).first_or_404()
    _uuid_id = uuid.uuid4()
    
    if action == 'like':
        current_user.like_post(post)
        db.session.commit()
        payload = {"record_id": post_id, "user_id":current_user.id, "likes":int(1), "timestamp": str(LocalTimezoneManilaModelFunc())}
        current_app.elasticsearch.index(index="clikes", id=_uuid_id, body=payload)
    if action == 'unlike':
        current_user.unlike_post(post)
        db.session.commit()
        body = {  "query" :  {
                        "bool": {
                        "must": [ {"match": {"record_id": post_id}}
                            ,{"match": {"user_id": current_user.id}}  ]
               } }}

        current_app.elasticsearch.delete_by_query(index='clikes', body=body)
    return redirect(request.referrer)


@bp.route('/static/pdfjs/web/viewer.html')
def show_pdfjs():
    #http://127.0.0.1:11414/static/pdfjs/web/viewer.html?file=/static/doc/1481953691.pdf
    return send_file('./static/pdfjs/web/viewer.html')



@bp.route("/view/<int:post_id>")  # this is ordniary post, no login required or should be (READ)
@bp.route("/view/<int:post_id>/")  # this is ordniary post, no login required or should be (READ)
def view(post_id):
    post = Post.query.get_or_404(post_id, description=f'No data found with {post_id}. The requested URL was not found \
                        on the server. If you entered the URL manually please check your spelling and try again.')

    post_record_stat = RecordStat.query.filter_by(record_id=post_id).first()

    plikes = Post.query.filter_by(id=post_id).all()



    if not post_record_stat:
        data = {'Record Statistics' : 'Behavior', 'Views' : 0, 'Likes' : 0}
    elif (post_record_stat.total_likes == None or post_record_stat.total_likes == '') and (post_record_stat.total_views != None or post_record_stat.total_views != ''):
        data = {'Record Statistics' : 'Behavior', 'Views' : post_record_stat.total_views, 'Likes' : 0}
    else:
        data = {'Record Statistics' : 'Behavior', 'Views' : post_record_stat.total_views, 'Likes' : post_record_stat.total_likes}

    
    post_content_recommended = db.session.query(ContentRecommendation.record_id, ContentRecommendation.recommended_content_id, Post.id, Post.title, Post.body)\
        .join( Post, (Post.id.cast(String) == ContentRecommendation.recommended_content_id.cast(String)))\
            .filter(ContentRecommendation.record_id == post_id).order_by(func.random()).limit(5)

    post_id_in_recommend_recommend = db.session.query(Post.id,func.concat(func.left(Post.title, 25), '...').label('title') , ContentRecommendation.record_id, ContentRecommendation.recommended_content_id, PopularityRecommendation.total_click_environment )\
        .join( Post, (Post.id.cast(String) == ContentRecommendation.recommended_content_id.cast(String)))\
        .join( PopularityRecommendation, (PopularityRecommendation.click_article_id.cast(String) == ContentRecommendation.recommended_content_id.cast(String)))\
        .filter(ContentRecommendation.record_id == post_id).order_by(func.random()).limit(10)


    bb_pp_big = [['Title', 'Total Views', { 'role': 'annotation' } ]]
    for id, title, record_id, recommended_content_id, total_click_environment  in post_id_in_recommend_recommend:
        bb_pp = [title, total_click_environment, id]
        bb_pp_big.append(bb_pp)


    sql_tree = f"""
    SELECT concat_ws(' ', p.title, substr(p.body, 1, 1500), 
			 unnest(p.community_type) ) as descriptions
    FROM public.content_recommendation c
    INNER JOIN public.post p
        ON p.id::varchar = c.recommended_content_id::varchar
		where c.record_id::int = {post_id}
        ORDER BY random()
        LIMIT 15
    """
    tree_post_map = db.session.execute(sql_tree)

    tree_post_mapped = [['Phrases']]
    for description in tree_post_map:
        tree_post_mapped.append([str(description).replace(')', '')\
                    .replace('(', '').replace('"', "").replace('\'', '')\
                    .replace('\n', '').replace('\t', '')\
                    .replace('.', '').replace(',', '')\
                    .replace(';', '').replace(' of ', '')\
                    .replace('The ', '').replace(' on ', '')\
                    .replace(' and ', '').replace(' the ', '')\
                    .replace(' are ', '').replace(' is ', '')\
                    .replace(' in ', '').replace(' for ', '')\
                    .replace(' to ', '').replace(' or ', '')\
                    .replace(' by ', '').replace('+', '')\
                    .replace(':', '').replace('%', '')\
                    ])


    
    attach_doc = FilesFiles.query.filter_by(record_id=post_id, storage_class='S').all()
    static_file_path_iframe = url_for('static', filename='assets/')
    
    return render_template('post.html', title=post.title, post=post, static_file_path_iframe=static_file_path_iframe, \
                            attach_doc=attach_doc, post_record_stat=post_record_stat, data=data, \
                            post_content_recommended=post_content_recommended,\
                            post_id_in_recommend_recommend=post_id_in_recommend_recommend,\
                            bb_pp_big=bb_pp_big, tree_post_mapped=tree_post_mapped, plikes=plikes)



@bp.route("/post/<int:post_id>")
@bp.route("/post/<int:post_id>/")
@login_required
def post(post_id):
    post = Post.query.get_or_404(post_id, description=f'No data found with {post_id}. The requested URL was not found \
                        on the server. If you entered the URL manually please check your spelling and try again.')

    post_record_stat = RecordStat.query.filter_by(record_id=post_id).first()

    plikes = Post.query.filter_by(id=post_id).all()

    if not post_record_stat:
        updated_likes = 0
    else:
        updated_likes =  post_record_stat.total_likes


    if not post_record_stat:
        data = {'Record Statistics' : 'Behavior', 'Views' : 0, 'Likes' : 0}
    elif (post_record_stat.total_likes == None or post_record_stat.total_likes == '') and (post_record_stat.total_views != None or post_record_stat.total_views != ''):
        data = {'Record Statistics' : 'Behavior', 'Views' : post_record_stat.total_views, 'Likes' : 0}
    else:
        data = {'Record Statistics' : 'Behavior', 'Views' : post_record_stat.total_views, 'Likes' : post_record_stat.total_likes}

    
    post_content_recommended = db.session.query(ContentRecommendation.record_id, ContentRecommendation.recommended_content_id, Post.id, Post.title, Post.body)\
        .join( Post, (Post.id.cast(String) == ContentRecommendation.recommended_content_id.cast(String)))\
            .filter(ContentRecommendation.record_id == post_id).order_by(func.random()).limit(5)

    post_id_in_recommend_recommend = db.session.query(Post.id,func.concat(func.left(Post.title, 25), '...').label('title') , ContentRecommendation.record_id, ContentRecommendation.recommended_content_id, PopularityRecommendation.total_click_environment )\
        .join( Post, (Post.id.cast(String) == ContentRecommendation.recommended_content_id.cast(String)))\
        .join( PopularityRecommendation, (PopularityRecommendation.click_article_id.cast(String) == ContentRecommendation.recommended_content_id.cast(String)))\
        .filter(ContentRecommendation.record_id == post_id).order_by(func.random()).limit(10)


    bb_pp_big = [['Title', 'Total Views', { 'role': 'annotation' } ]]
    for id, title, record_id, recommended_content_id, total_click_environment  in post_id_in_recommend_recommend:
        bb_pp = [title, total_click_environment, id]
        bb_pp_big.append(bb_pp)


    sql_tree = f"""
    SELECT concat_ws(' ', p.title, substr(p.body, 1, 1500), 
			 unnest(p.community_type) ) as descriptions
    FROM public.content_recommendation c
    INNER JOIN public.post p
        ON p.id::varchar = c.recommended_content_id::varchar
		where c.record_id::int = {post_id}
        ORDER BY random()
        LIMIT 15
    """
    tree_post_map = db.session.execute(sql_tree)

    tree_post_mapped = [['Phrases']]
    for description in tree_post_map:
        tree_post_mapped.append([str(description).replace(')', '')\
                    .replace('(', '').replace('"', "").replace('\'', '')\
                    .replace('\n', '').replace('\t', '')\
                    .replace('.', '').replace(',', '')\
                    .replace(';', '').replace(' of ', '')\
                    .replace('The ', '').replace(' on ', '')\
                    .replace(' and ', '').replace(' the ', '')\
                    .replace(' are ', '').replace(' is ', '')\
                    .replace(' in ', '').replace(' for ', '')\
                    .replace(' to ', '').replace(' or ', '')\
                    .replace(' by ', '').replace('+', '')\
                    .replace(':', '').replace('%', '')\
                    ])


    
    attach_doc = FilesFiles.query.filter_by(record_id=post_id, storage_class='S').all()
    static_file_path_iframe = url_for('static', filename='assets/')
    
    payload = {"record_id": post_id, "user_id":current_user.id, "views":int(1), "timestamp": str(LocalTimezoneManilaModelFunc())}
    current_app.elasticsearch.index(index="cstat", id=uuid.uuid4(), body=payload)
    return render_template('post.html', title=post.title, post=post, static_file_path_iframe=static_file_path_iframe, \
                            attach_doc=attach_doc, post_record_stat=post_record_stat, data=data, \
                            post_content_recommended=post_content_recommended,\
                            post_id_in_recommend_recommend=post_id_in_recommend_recommend,\
                            bb_pp_big=bb_pp_big, tree_post_mapped=tree_post_mapped, plikes=plikes, updated_likes=updated_likes)




@bp.route('/index')
@bp.route('/')
#@login_required
def index():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.modified_date.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.index', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.index', page=posts.prev_num) \
        if posts.has_prev else None
    

    popular_community_slice_pie = db.session.query(func.unnest(Post.community_type).label('community') , func.sum(PopularityRecommendation.total_click_environment).label('value') )\
        .join( Post, (Post.id.cast(String) == PopularityRecommendation.click_article_id.cast(String))
        ).group_by(func.unnest(Post.community_type))\
        .distinct().all()
    

    popular_community_sliced = [['Community', 'Total Views']]
    for community, value in popular_community_slice_pie:
        popular_community_sliced.append([community.title(), int(value)])
    popular_community_sliced = [[v if v is not None else 0 for v in nested] for nested in popular_community_sliced]


    
    sql = text("""
    select a.year, sum(a.anno) anno, sum(a.banana) banana, sum(a.cacao) cacao, sum(a.coconut) coconut,
        sum(a.coffee) coffee, sum(a.corn) corn, sum(a."event") "event", sum(a.other) other, sum(a.rice) rice,
        sum(a.soybean) soybean, sum(a.sugarcane) sugarcane, sum(a.tech) tech, sum(a.tomato) tomato
    from (

        SELECT *
        FROM crosstab($$
            WITH CTE AS (
            select  substr(pr.date::varchar, 1,6)::int as date,
                unnest(p.community_type) as community,
                coalesce(sum(pr.total_click_environment), 0) as total_sum,
                ROW_NUMBER() OVER (PARTITION BY unnest(p.community_type) ORDER BY substr(pr.date::varchar, 1,6)::int DESC) rn  
            from public.popularity_recommendation pr
            JOIN public.post p 
                on p.id = pr.click_article_id
            GROUP BY  substr(pr.date::varchar, 1,6)::int, unnest(p.community_type)
            )
            SELECT to_char(to_date(date::varchar, 'YYYYMMDD') , 'Mon YYYY'), 
                    community, total_sum 
            FROM CTE 
            WHERE rn <=5 
            ORDER BY community;
        $$,
        $$ SELECT a.* FROM (SELECT DISTINCT id as community FROM public.communities_community) a WHERE a.community != 'all'
         ORDER BY 1; $$
        )
        AS ct(
            year       varchar,
            "anno"     int,
            "banana"   int,
            "cacao"    int,
            "coconut"  int,
            "coffee"   int,
            "corn"     int,
            "event"    int,
            "other"    int,
            "rice"     int,
            "soybean"  int,
            "sugarcane"int,
            "tech"     int,
            "tomato"   int
        )
    ) a group by a.year;
    """)
    popular_community_result = db.session.execute(sql)

    m_community = ['Date', 'Announcement', 'Banana', 'Cacao', 'Coconut', 'Coffee', 'Corn', 'Event', 'Others', 'Rice', 'Soybean', 'Sugarcane', 'Technology', 'Tomato']
    
    mm_pp_big = [m_community]
    for date, anno, banana, cacao, coconut, coffee, corn, event, others, rice, soybean, sugarcane, tech, tomato in popular_community_result:
        mm_pp = [date, anno, banana, cacao, coconut, coffee, corn, event, others, rice, soybean, sugarcane, tech, tomato]
        mm_pp_big.append(mm_pp)
    mm_pp_big = [[v if v is not None else 0 for v in nested] for nested in mm_pp_big]



    sql_title_likes_views = text("""
    WITH TV AS 
        (SELECT record_id, total_views
        FROM public.record_stat
        ORDER BY total_views desc NULLS LAST
        LIMIT 20)
    , TL AS
        (SELECT record_id, total_likes
        FROM public.record_stat
        ORDER BY total_likes desc NULLS LAST
        LIMIT 20 )
    , TL_TV AS (
        SELECT COALESCE(TV.record_id, TL.record_id) as record_id,
            COALESCE(TV.total_views, 0) as total_views,
            COALESCE(TL.total_likes, 0) as total_likes
        FROM TV 
        FULL OUTER JOIN TL
            ON TL.record_id = TV.record_id
    )
    SELECT b.title, a.total_views, a.total_likes, b.id FROM TL_TV a INNER JOIN public.post b
        ON a.record_id::varchar = b.id::varchar
        ORDER BY a.total_views DESC, a.total_likes DESC;
    """)
    popular_title_likes_views = db.session.execute(sql_title_likes_views)

    
    mm_pp_big_title = [['title', 'Total Like', 'Total Views']]
    for title, total_likes, total_views, id in popular_title_likes_views:
        mm_pp_tt = [title, total_likes, total_views]
        mm_pp_big_title.append(mm_pp_tt)
    mm_pp_big_title = [[v if v is not None else 0 for v in nested] for nested in mm_pp_big_title]



    return render_template('index.html', title=_('Home'),
                           posts=posts.items, next_url=next_url,
                           prev_url=prev_url, popular_community_type=mm_pp_big,
                           popular_community_sliced=popular_community_sliced,
                           mm_pp_big_title = mm_pp_big_title
                           )



@bp.route("/post/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403) 
    if request.method == 'POST':
        try:
            db.session.delete(post)
            db.session.query(FilesFiles).filter(FilesFiles.record_id == post_id).update({'storage_class': 'X'})
            db.session.commit()
            current_app.elasticsearch.delete(index='post', id=post_id)
            flash('Your post has been deleted!', 'success')
            return redirect(url_for('main.index'))
        except:
            flash('Your post has been deleted with no index', 'warning')
            return redirect(url_for('main.index'))

        

@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = user.posts.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.user', username=user.username,
                       page=posts.next_num) if posts.has_next else None
    prev_url = url_for('main.user', username=user.username,
                       page=posts.prev_num) if posts.has_prev else None
    form = EmptyForm()
    return render_template('user.html', user=user, posts=posts.items,
                           next_url=next_url, prev_url=prev_url, form=form)


@bp.route('/user/<username>/popup')
@login_required
def user_popup(username):
    user = User.query.filter_by(username=username).first_or_404()
    form = EmptyForm()
    return render_template('user_popup.html', user=user, form=form)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash(_('Your changes have been saved.'))
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title=_('Edit Profile'),
                           form=form)


@bp.route('/edit_profile/personalized_notif', methods=['GET', 'POST'])
@login_required
def edit_personalized_notifications():
    pn_set = PersonalizedNotificationInfo.query.filter_by(user_id=current_user.id).first_or_404()

    form = PersonalizedNotificationForm()
    community_groups = db.session.query(CommunitiesCommunity.id, CommunitiesCommunity.title)\
        .filter(CommunitiesCommunity.deleted_at == None) \
        .order_by(CommunitiesCommunity.title.desc())
    form.community_pn.choices = [(c.id, c.title) for c in community_groups]

    material_groups = db.session.query(RecordsRecordsMaterialOrPublicationType.id, \
        RecordsRecordsMaterialOrPublicationType.material_publication_type)\
        .filter(RecordsRecordsMaterialOrPublicationType.deleted != True) \
        .order_by(RecordsRecordsMaterialOrPublicationType.id.asc())
    form.material_pub_rdo_btn_pn.choices = [(m.material_publication_type, m.material_publication_type.title())for m in material_groups]
    

    if form.validate_on_submit():
        pn_set.community_type            = request.form.getlist('community_pn') # form.community_pn.data
        pn_set.material_publication_type = form.material_pub_rdo_btn_pn.data
        pn_set.activate_pn               = form.activate_pn.data
        db.session.commit()
        flash(_('Your changes have been saved.'))
        return redirect(url_for('main.edit_personalized_notifications'))

    elif request.method == 'GET':
        form.community_pn.data             = pn_set.community_type
        form.material_pub_rdo_btn_pn.data   = pn_set.material_publication_type
        form.activate_pn.data               = pn_set.activate_pn
    return render_template('edit_personalized_notif.html', title=_('Personalized your Notifications'),
                           form=form)


@bp.route('/follow/<username>', methods=['POST'])
@login_required
def follow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        if user is None:
            flash(_('User %(username)s not found.', username=username))
            return redirect(url_for('main.index'))
        if user == current_user:
            flash(_('You cannot follow yourself!'))
            return redirect(url_for('main.user', username=username))
        current_user.follow(user)
        db.session.commit()
        flash(_('You are following %(username)s!', username=username))
        return redirect(url_for('main.user', username=username))
    else:
        return redirect(url_for('main.index'))


@bp.route('/unfollow/<username>', methods=['POST'])
@login_required
def unfollow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        if user is None:
            flash(_('User %(username)s not found.', username=username))
            return redirect(url_for('main.index'))
        if user == current_user:
            flash(_('You cannot unfollow yourself!'))
            return redirect(url_for('main.user', username=username))
        current_user.unfollow(user)
        db.session.commit()
        flash(_('You are not following %(username)s.', username=username))
        return redirect(url_for('main.user', username=username))
    else:
        return redirect(url_for('main.index'))


@bp.route('/translate', methods=['POST'])
@login_required
def translate_text():
    return jsonify({'text': translate(request.form['text'],
                                      request.form['source_language'],
                                      request.form['dest_language'])})


@bp.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.index'))
    page = request.args.get('page', 1, type=int)
    posts, total = Post.search(g.search_form.q.data, page,
                               current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title=_('Search'), posts=posts,
                           next_url=next_url, prev_url=prev_url)

@bp.route('/msearch')
@login_required
def msearch():
    page = request.args.get('page', 1, type=int)
    posts, total = Post.search(g.search_form.q.data, page,
                               current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('main.msearch', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.msearch', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title=_('Search'), posts=posts,
                           next_url=next_url, prev_url=prev_url)



@bp.route('/send_message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = User.query.filter_by(username=recipient).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user,
                      body=form.message.data)
        db.session.add(msg)
        user.add_notification('unread_message_count', user.new_messages())
        db.session.commit()
        flash(_('Your message has been sent.'))
        return redirect(url_for('main.user', username=recipient))
    return render_template('send_message.html', title=_('Send Message'),
                           form=form, recipient=recipient)


@bp.route('/messages')
@login_required
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()).paginate(
            page, current_app.config['POSTS_PER_PAGE'], False)

    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return render_template('messages.html', messages=messages.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/export_posts')
@login_required
def export_posts():
    if current_user.get_task_in_progress('export_posts'):
        flash(_('An export task is currently in progress'))
    else:
        current_user.launch_task('export_posts', _('Exporting posts...'))
        db.session.commit()
    return redirect(url_for('main.user', username=current_user.username))


@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications])


